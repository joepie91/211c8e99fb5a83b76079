Promise.try(function(){
	return outerThingOne();
}).then(function(value){
	return Promise.try(function(){
		return innerThingOne();
	}).then(function(subValue){
		return innerThingTwo(subValue);
	});
}).then(function(result){
	return outerThingThree(result);
});

/* ... is equivalent to ... */

Promise.try(function(){
	return outerThingOne();
}).then(function(value){
	return innerThingOne();
}).then(function(subValue){
	return innerThingTwo(subValue);
}).then(function(result){
	return outerThingThree(result);
});